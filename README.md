# GC Win10 Scripts


## Description
This scripts are destened to automate and simplify some processes in work with Windows 10 for Game Club PC's.

## Installation
Every script can work apart from other scripts. List of required files for script check in text of script.
To start main-script you need to create folder "C:/Scripts" and open Powershell with Admin then write this commands:
```powershell
Set-ExecutionPolicy Unrestricted;
```
```powershell
powershell -file C:/Scripts/main-script.ps1
```
```powershell
Set-ExecutionPolicy Default;
```
Note: Before start winget-script.ps1, be sure that you have installed Winget on your computer. If you don't have Winget on your computer you can download Winget from https://aka.ms/getwingetpreview, after that you can drop install-file to usb and turn-on using installers-from-flashusb-script.ps1.
Maybe you will need to uncommit some commands in text of main-script.


## Usage
### Tools
1. WinGet - https://learn.microsoft.com/en-us/windows/package-manager/winget/
2. Powershell
3. Your Flash-usb (optional)

## Support
mail: pavletsovgames@gmail.com

## Roadmap
I have a plan and I'm sticking to it.

## Authors and acknowledgment
salamandr_bash

## License
No License.

## Project status
Sometimes I will add scripts.
