#Удаление ненужных программ
$path2RmApps = "rem-apps.txt"
#Проверка на доступность файла
if (Test-Path $path2RmApps) {
    $removeNames = Get-Content $path2RmApps

    #Перебор названий и удаление
    foreach ($removeName in $removeNames) {
         Get-Appxpackage *$removeName* | Remove-Appxpackage
    }
}