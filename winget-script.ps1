echo "winget-script is start work..."
$idListPath = "C:/Scripts/winget-id-list.txt"

#Проверка на адектватность
if (Test-Path $idListPath) {
    #Считывание данных файла
    $idList = Get-Content $idListPath

    foreach ($idItem in $idList) {
        echo ("Now will installig of $idItem")
        winget install --id=$idItem -e;
    }
} else {
    Write-Host "Error:  Can't find file with WinGet-ID..."
}