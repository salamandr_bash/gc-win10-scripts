# Разрешение выполнять, не подписанные цифровой подписью скрипты
Set-ExecutionPolicy Unrestricted;

# Активатор Windows
#irm https://massgrave.dev/get | iex;

# Отключение автообновлений
powershell -file C:/Scripts/disable-win10-autoupdates-script.ps1;

# Удаление ненужных программ
#powershell -file C:/Scripts/rm-items-script.ps1;

# Установка программ с флешки
##installers-from-flashusb-script.ps1

#Установка необходимых программ
powershell -file C:/Scripts/winget-script.ps1;