# Примечание: Для работы скрипта нужно создать рядом со скриптом файл "installers-usb-path.txt" и в нём записать все пути к установочникам 
# Путь к файлу с путями к установочным файлам
$pathListFile = "C:\Scripts\installers-usb-paths.txt"

# Проверка наличия файла с путями
if (Test-Path $pathListFile) {
    # Чтение списка путей из файла
    $installerPaths = Get-Content $pathListFile

    # Проверка каждого пути и запуск установочных файлов поочередно
    foreach ($installerPath in $installerPaths) {
        # Проверка наличия установочного файла
        if (Test-Path $installerPath) {
            # Запуск установочного файла
            Start-Process -FilePath $installerPath -Wait
        } else {
            Write-Host "Error: Can't find file with path $installerPath."
        }
    }
} else {
    Write-Host "Error: Can't find file with path's to installers."
}