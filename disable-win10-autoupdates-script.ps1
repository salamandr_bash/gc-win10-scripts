# Отключение автоматических обновлений Windows 10 Pro

# Проверяем, является ли текущий пользователь администратором
$isAdministrator = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")

if (-not $isAdministrator) {
    Write-Host "You don't have permission to start this script"
    exit
}

# Отключаем службу обновления Windows
Write-Host "Disabling auto-updates Windows..."
Set-Service -Name wuauserv -StartupType Disabled
Stop-Service -Name wuauserv

Write-Host "Windows automatic updates are disable."

# Для проверки заходим в gpedit.msc -> Конфигурация компьютера\Административные шаблоны\Компоненты Windows\Центр обновления Windows В правой части окна выберите пункт «Настройка автоматического обновления» и в нём состояние должно быть "Отключено"